function addStudent() {
    var modal = document.getElementById("myModal2");
    modal.style.display = "block";

    var confirmBtn = document.getElementById("confirmBtn");
    confirmBtn.onclick = function() {
    let table = document.getElementById("tbl");
    let row = table.insertRow(table.length);
    let cell1 = row.insertCell(0);
    let cell2 = row.insertCell(1);
    let cell3 = row.insertCell(2);
    let cell4 = row.insertCell(3);
    let cell5 = row.insertCell(4);
    let cell6 = row.insertCell(5);
    let cell7 = row.insertCell(6);
    let checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.onclick = function (){
        changeCircleColor(cell6);
    }
    cell1.appendChild(checkbox);
    cell1.appendChild(checkbox);
    cell2.innerHTML = "PZ-25"
    cell3.innerHTML = "Daria Kyselytsia"
    cell4.innerHTML = "F"
    cell5.innerHTML = "23.06.2004"
    cell6.innerHTML = '<div class="greyCircle"></div>';
    cell6.style = "padding: 0; width: 20px; padding-left: 40px";
    cell7.innerHTML = '<button style = "margin-right: 1%; width: 25px; height: 25px; padding: 0; display: flex; justify-content: center">' +
        '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16" style="margin-top: 3px">\n' +
        '  <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>\n' +
        '</svg></button>' +
        '<button style = "margin-right: 1%; width: 25px; height: 25px"; onclick="removeStudent(this.parentNode.parentNode)">'+
        '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16" style="margin-left: -2.5px; margin-top: -3px;">'+
        '<path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>'+
        '<path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>'+
       '</svg> </button>'
    cell7.style = "display: flex; border: 0; justify-content: center; padding-top: 20px";

modal.style.display = "none";
}

var cancelBtn = document.getElementById("cancelBtn");
cancelBtn.onclick = function() {
  modal.style.display = "none";
}
}

function removeStudent(el) {
    if(confirm("Are you sure you want to delete user?"))
        el.remove();
}

function changeCircleColor(tableCell){
    let innerHTML = tableCell.innerHTML;
    if(innerHTML.match(new RegExp("grey"))){
        tableCell.innerHTML = "<div class=\"greenCircle\"></div>";
    }else{
        tableCell.innerHTML = "<div class = \"greyCircle\"></div>";
    }
}